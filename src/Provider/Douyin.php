<?php


namespace Qypt\PhpWm\Provider;

use Qypt\PhpWm\HttpClient\DouyinHttpClient;
use Qypt\PhpWm\ProviderInterface;
use QL\QueryList;
use GuzzleHttp\Client;

/**
 * Douyin.
 *
 * @package Qypt\PhpWm\Analysis
 *
 * qypt
 */
class Douyin implements ProviderInterface
{
    public $html = null;
    public $data = null;
    public $url = null;

    public function __construct(string $url)
    {

        $this->setUrl($url);
        return $this;
    }

    public function setUrl(string $url)
    {
        $this->url = $url;
        $this->html = DouyinHttpClient::get($url);
        $this->data = $this->getDouyin();
    }

    private function getVideoId()
    {
        preg_match('/href="(.*?)">Found/', $this->html, $matches);
        $url_share = $matches[1];
        preg_match('/video\/(.*?)\//', $url_share, $matches);
        return $matches[1];
    }

    private function getDouyin()
    {
        /*if (is_null($this->data)) {
            $videoId = $this->getVideoId();
            $url = "https://www.douyin.com/video/{$videoId}";
            $client = new Client();
            $res = $client->request('GET', $url, [
                'query' => ['wd' => 'QueryList'],
                'headers' => [
                    'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
                    'Accept-Encoding' => 'gzip, deflate, br',
                    'Accept-Language' => ' zh-CN,zh;q=0.9',
                ]
            ]);
            $html = (string)$res->getBody();
            var_dump($html);die;
            $result = array();
            $p2 = '/<script id="RENDER_DATA" type="application\/json">([\w\W]*)<\/script><script/iU';
            if(preg_match_all( $p2, $html, $result) )
            {
                if(isset($result[1][0])){
                    $aa = urldecode($result[1][0]);
                    $arr = json_decode($aa);
                }
            }
            // $this->data = json_decode($json, true);
        }*/
        if (is_null($this->data)) {
            $videoId = $this->getVideoId();
            $url = "https://www.douyin.com/video/{$videoId}";
            $urlOld = "https://www.douyin.com/web/api/v2/aweme/iteminfo/?item_ids=".$videoId;
            $urlOld = "https://www.iesdouyin.com/web/api/v2/aweme/iteminfo/?item_ids=".$videoId;
            $cookie = 'passport_csrf_token=7b312998941b26e50a504388539d1590; passport_csrf_token_default=7b312998941b26e50a504388539d1590; ttwid=1%7Ce27WNke0ME_c2bETcfMZx6D4gjaGC4oc1J2OuwssWAg%7C1670733288%7Cbdbac19e355633aac8ea017593a9872788ba8331d11cb8e26e5ac19803157cb6; douyin.com; s_v_web_id=verify_lckieujt_bltk6ykD_lKKb_416B_9BaN_RPmmfVYmEAmC; _tea_utm_cache_2018=undefined; __ac_nonce=063b841ef000edd783262; __ac_signature=_02B4Z6wo00f0135241wAAIDD.nQZHXWLH1N-VufAALwxLxrRm686Jwi3m4x-Slgd5GXcH0vwk2GJH0NoMydS1jb39fUlrXmUFvq-tqH1Bum5OJKKgCl.eu0J2-PnWqbwJ1q0Wbrhqcie5rFLf7; home_can_add_dy_2_desktop=%221%22; tt_scid=-L0nheO1r0eEF62Gz3ksMdmhw7X82ssPaPLBxqDq7ekTsuBUbX3fxL5xFbzh8DAf05fa; msToken=gVTf-YVXCojg5Zb6RqkSe6gRpQk43ZT4NYeaku78tpCn4J4Wl3j_lbSiVfwXlH9tTtbG-4v4V_Z0xrqi0Cck2sbtO0-lMlN7IWKXJtIz_LiGDebXOcr5TJnIdmFAUtwI; msToken=I4wKclu5QZPcBotMK8Ob0AQZtKfkGT-tRcD8djLjh_AdW8A9iC3sp_P_8_8r1WhJ6ryMlenH8ZceVzRfT7SSVTxabExmuw7omnIsiey9vigekkwjD1dCfQGTAvpAC9UI';


            $head = [
                'user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
                ':authority: www.douyin.com',
                ':method: GET',
                ":path: /web/api/v2/aweme/iteminfo/?item_ids={$videoId}",
                ':scheme: https',
                'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                "cookie:{$cookie}",
                'sec-ch-ua: " Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"',
                'sec-ch-ua-mobile: ?0',
                'sec-ch-ua-platform: "Windows"',
                'sec-fetch-dest: document',
                'sec-fetch-mode: navigate',
                'sec-fetch-site: none',
                'sec-fetch-user: ?1',
                'upgrade-insecure-requests: 1',
            ];


//            DouyinHttpClient::postRequest('https://mssdk.bytedance.com/web/report');
//            DouyinHttpClient::postRequest('https://mon.zijieapi.com/monitor_browser/collect/batch/');
//            DouyinHttpClient::postRequest('https://mcs.zijieapi.com/list');

            $json = DouyinHttpClient::getRequest($urlOld);


            var_dump($urlOld);die;
            $this->data = json_decode($json, true);
        }
        return $this->data;
    }

    public function getData()
    {
        return [
            'title' => $this->getTitle(),
            'url' => $this->getUrl(),
            'img' => $this->getImg(),
            'music' => $this->getMusic(),
        ];
    }

    public function getMusic()
    {
        $this->getDouyin();
        return $this->data['item_list'][0]['music']['play_url']['url_list'][0];
    }

    public function getImg()
    {
        $this->getDouyin();
        return $this->data['item_list'][0]['video']['origin_cover']['url_list'][0];
    }
    public function getTitle()
    {
        $this->getDouyin();
        return $this->data['item_list'][0]['desc'];
    }

    public function getUrl()
    {
        $this->getDouyin();

        $vid = $this->data['item_list'][0]['video']['vid'] ?? "";

        if ($vid == '') {
            return '';
        }

        $ratio = $this->data['item_list'][0]['video']['ratio'] ?? '540p';

        $link = "https://aweme.snssdk.com/aweme/v1/play/?video_id={$vid}&line=0&ratio={$ratio}&media_type=4&vr_type=0&improve_bitrate=0&is_play_url=1&is_support_h265=0&source=PackSourceEnum_PUBLISH";

        return $link;
    }
}
