<?php

namespace Qypt\PhpWm\Provider;

use Qypt\PhpWm\HttpClient\HttpClient;
use Qypt\PhpWm\ProviderInterface;

/**
 * Class Kuaishou.
 *
 * @package Qypt\PhpWm\Provider
 *
 * qypt
 */
class Kuaishou implements ProviderInterface
{
    public $html = null;
    public $data = null;
    public $url = null;

    public function __construct(string $url)
    {

        $this->setUrl($url);
        return $this;
    }

    public function setUrl_new(string $url)
    {

        // TODO: Implement setUrl() method.
        $this->url = $url;

        $headers = [
            'Connection' => 'keep-alive',
            'User-Agent'=>'Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/16D57 Version/12.0 Safari/604.1'
        ];

        $client = new \GuzzleHttp\Client(['timeout' => 2, 'headers' => $headers, 'http_errors' => false,]);
        $data['headers'] = $headers;
        $jar = new \GuzzleHttp\Cookie\CookieJar;
        $data['cookies'] = $jar;

        $response = $client->request('GET', $url, $data);
        dp($response);
        $body = $response->getBody();
        if ($body instanceof \GuzzleHttp\Psr7\Stream) {
            $body = $body->getContents();
        }
        $result = htmlspecialchars_decode($body);
        $pattern = '#"srcNoMark":"(.*?)"#';
        preg_match($pattern, $result, $match);
        $data['video_src'] = $match[1];
        $pattern = '#"poster":"(.*?)"#';
        preg_match($pattern, $result, $match);
        if (!empty($match[1])) {
            $data['cover_image'] = $match[1];
            echo json_encode($data);die();
        }

    }


    public function setUrl(string $url)
    {
        $this->url = $url;

        $options = [
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Linux; Android 8.0.0; Pixel 2 XL Build/OPD1.170816.004) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Mobile Safari/537.36',
                'Cookie' => 'did=web_e19cfe96787746a5b4311f56acaf5be4; didv=1593660908000; clientid=3; client_key=65890b29; Hm_lvt_86a27b7db2c5c0ae37fee4a8a35033ee=1594966818,1595649199; sid=6281151b7052a88cfaba3a43'
            ]
        ];

       $options = [
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36',
                'Cookie' => 'did=web_6969b67f0724478abe2e3c9d0f625ee7; didv=1668357346000; kpf=PC_WEB; kpn=KUAISHOU_VISION; clientid=3',
                'Origin' => 'https://www.kuaishou.com',
                'Sec-Fetch-Site' => 'same-origin',
                'Sec-Fetch-Mode' => 'cors',
                'Sec-Fetch-Dest' => 'empty',
                'Host' => 'www.kuaishou.com'
            ]
        ];
//        Accept-Encoding: gzip, deflate, br
//Accept-Language: zh-CN,zh;q=0.9
//Connection: keep-alive
//Content-Length: 342
//content-type: application/json
//Cookie: did=web_6969b67f0724478abe2e3c9d0f625ee7; didv=1668357346000; kpf=PC_WEB; kpn=KUAISHOU_VISION; clientid=3
//Host: www.kuaishou.com
//Origin: https://www.kuaishou.com
//Referer: https://www.kuaishou.com/short-video/3xffjwjx6ik7nu9?userId=3xjngx9hcxg9xvs
//Sec-Fetch-Dest: empty
//        Sec-Fetch-Mode: cors
//Sec-Fetch-Site: same-origin
//User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36
//
        //$url = 'https://v.m.chenzhongtech.com/fw/photo/3x53quws7ek9hts?fid=2050667985&cc=share_copylink&followRefer=151&shareMethod=TOKEN&docId=9&kpn=KUAISHOU&subBiz=BROWSE_SLIDE_PHOTO&photoId=3x53quws7ek9hts&shareId=17210073598418&shareToken=X9xNXdFColaB1gT&shareResourceType=PHOTO_OTHER&userId=3xjngx9hcxg9xvs&shareType=1&et=1_i%2F2001729856734404018_d310ods%24s&shareMode=APP&originShareId=17210073598418&appType=21&shareObjectId=5204190927703489883&shareUrlOpened=0&timestamp=1668357013269';

        $this->html = HttpClient::get($url, $options);


        preg_match('/window\.pageData= ([\s\S]*?)<\/script>/i', $this->html, $matches);

        $this->data = json_decode($matches[1] ?? '', true);

        dp($this->html);
    }



    public function getData()
    {
        return [
            'title' => $this->getTitle(),
            'url' => $this->getUrl(),
            'img' => $this->getImg(),
            'music' => $this->getMusic(),
        ];
    }
    public function getTitle()
    {
        return $this->data['video']['caption'] ?? '';
    }
    public function getImg()
    {
        return $this->data['video']['poster'] ?? '';
    }
    public function getUrl()
    {
        return $this->data['video']['srcNoMark'] ?? '';
    }
    public function getMusic()
    {
        return $this->data['rawPhoto']['music']['url'] ?? '';
    }
}
