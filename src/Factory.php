<?php

namespace Qypt\PhpWm;

use Qypt\PhpWm\Provider\Douyin;
use Qypt\PhpWm\Provider\Kuaishou;

/**
 * Factory.
 *
 * @package Qypt\PhpWm
 *
 * @method Douyin douyin(string $url)
 * @method Kuaishou kuaishou(string $url)
 *
 */


class Factory
{
    protected static $providers = [
        'douyin' => Douyin::class,
        'kuaishou' => Kuaishou::class
    ];

    /**
     * @param string $name
     * @param string $value
     *
     * @return ProviderInterface
     *
     * qypt
     */
    public static function make(string $name, string $value)
    {
        return new self::$providers[$name]($value);
    }

    /**
     * __callStatic.
     *
     * @param static $name
     * @param array $arguments
     *
     * @return Factory|ProviderInterface
     *
     * qypt
     */
    public static function __callStatic(string $name, $arguments)
    {
        return self::make($name, ...$arguments);
    }
}
