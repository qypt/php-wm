<?php


namespace Qypt\PhpWm;

/**
 * Interface ProviderInterface
 * @package Qypt\PhpWm
 */
interface ProviderInterface
{
    public function setUrl(string $url);
    public function getData();
    public function getTitle();
    public function getImg();
    public function getUrl();
    public function getMusic();
}
